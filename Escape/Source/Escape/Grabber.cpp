// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Initializing struct declared in the header file
struct UGrabber::PlayerViewPoint {
	// Player Location at this tick
	FVector ViewLocation;
	// Player Rotation at this tick
	FRotator ViewRotation;
	// Player Line trace at this tick
	FVector PlayerLineTraceEnd;
};

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsComponent();
	SetUpInpuComponent();
}

void UGrabber::SetUpInpuComponent()
{
	InputHandle = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputHandle) {
		UE_LOG(LogTemp, Warning, TEXT("Input ready for %s"), *(GetOwner()->GetName()));
		/// Bind the input axis
		InputHandle->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputHandle->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Input not found for %s"), *(GetOwner()->GetName()))
	}
}

void UGrabber::FindPhysicsComponent()
{
	/// Look for attached Phisics Handle
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle) {
		// Ho trovato la classe quindi non faccio nulla
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("No such class in %s component"), *(GetOwner()->GetName()));
	}
}

void UGrabber::Grab() {
	UE_LOG(LogTemp, Warning, TEXT("Grab pressed by %s"), *(GetOwner()->GetName()));

	/// LINE TRACE and see if we reach any actors with physics body collision channel set
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();

	// If we hit something then attach physics handle
		// attach physics handle
	if (ActorHit) {
		if (!PhysicsHandle) { 
			UE_LOG(LogTemp, Error, TEXT("Physics handle is null!")); 
			return;
		}
		PhysicsHandle->GrabComponentAtLocationWithRotation(ComponentToGrab, NAME_None, GetOwner()->GetTargetLocation(), GetOwner()->GetActorRotation());
	}
}

void UGrabber::Release() {
	UE_LOG(LogTemp, Warning, TEXT("Released pressed by %s"), *(GetOwner()->GetName()));
	// Release physics handle
	if (!PhysicsHandle) { 
		UE_LOG(LogTemp, Error, TEXT("Physics handle is null!"));
		return;
	}
	PhysicsHandle->ReleaseComponent();
}

// Method to get the player View Point line trace
UGrabber::PlayerViewPoint* UGrabber::GetPlayerLineTraceEnd() {
	// Initialize the pointer to the structure
	PlayerViewPoint* ViewPoint = new PlayerViewPoint();

	// Get the player view point at this tick
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT ViewPoint->ViewLocation, OUT ViewPoint->ViewRotation); // uso la macro out per capire che il getter mette il risultato del metodo dentro le mie variabili

	ViewPoint->PlayerLineTraceEnd = ViewPoint->ViewLocation + ViewPoint->ViewRotation.Vector() * Reach;

	// Return a pointer to the structure
	return ViewPoint;
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!PhysicsHandle) { 
		UE_LOG(LogTemp, Error, TEXT("Physics handle is null!"));
		return;
	}
	// if the physics handle is attached
		// move the object that we are holding
	if (PhysicsHandle->GrabbedComponent) {
		if (GetPlayerLineTraceEnd() == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("Line trace end is null!"));
			return;
		}
		PhysicsHandle->SetTargetLocationAndRotation(GetPlayerLineTraceEnd()->PlayerLineTraceEnd, GetPlayerLineTraceEnd()->ViewRotation);
	}
}

const FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	FHitResult Hit;

	// Setup query parameters
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

	// Line trace (AKA ray-cast) out to reach distance
	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		GetPlayerLineTraceEnd()->ViewLocation,
		GetPlayerLineTraceEnd()->PlayerLineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters
	);

	// See what we hit
	if (Hit.GetActor()) {
		UE_LOG(LogTemp, Warning, TEXT("%s is colliding with %s"), *(GetOwner()->GetName()), *(Hit.GetActor()->GetName()));
	}

	return Hit;
}

