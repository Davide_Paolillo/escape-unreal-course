// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	float Reach = 100.0f;
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputHandle = nullptr;
	typedef struct PlayerViewPoint;
	// Ray-cast and grab what is in reach
	void Grab();
	// Called when grab key is released
	void Release();
	// Find attached input components
	void SetUpInpuComponent();
	// Find attached physic components
	void FindPhysicsComponent();
	// Get the first physics body in reach
	const FHitResult GetFirstPhysicsBodyInReach();
	// Get the Player view point line trace
	PlayerViewPoint* GetPlayerLineTraceEnd();

};
